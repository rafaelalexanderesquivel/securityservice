package mx.com.bbva.web.rest;

import io.swagger.annotations.Api;
import mx.com.bbva.commons.to.UserTO;
import mx.com.bbva.services.facade.IsecurityFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping("security")
@Api(value = "security", description = "Operaciones con security")
public class HelloController {

    static final Logger LOG = LogManager.getLogger(HelloController.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    IsecurityFacade IsecurityFacade;

    @RequestMapping(value = "/find", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<UserTO>> getAllUsers() {
        LOG.info("Se invoca /find");
        List<UserTO> users = this.IsecurityFacade.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity test() {
        LOG.info("Se invoca /test");

        ResponseEntity<String> responseEntity = restTemplate.exchange("http://catalogos-service/catalogos/users", HttpMethod.GET, HttpEntity.EMPTY, String.class);

        return new ResponseEntity<>(responseEntity, HttpStatus.OK);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity save(@RequestBody UserTO userTO) {
        LOG.info("Se invoca /save");
        restTemplate.postForEntity("http://catalogos-service/catalogos/user", userTO, UserTO.class);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}

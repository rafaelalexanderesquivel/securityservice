package mx.com.bbva.services.facade.impl;

import mx.com.bbva.commons.to.UserTO;
import mx.com.bbva.services.facade.IsecurityFacade;
import mx.com.bbva.services.service.IsecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class securityFacade implements IsecurityFacade {

    @Autowired
    private IsecurityService securityService;

    public List<UserTO> getAllUsers() {
        return this.securityService.getUsers();
    }
}
